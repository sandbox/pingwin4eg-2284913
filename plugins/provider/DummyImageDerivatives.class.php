<?php

/**
 * @file
 * CTools plugin dummy_image_derivatives class.
 */

/**
 * Dummy Image Derivatives class.
 */
class DummyImageDerivatives extends DevelImagesProviderBase {

  /**
   * Override parent with no settings form.
   */
  public function settingsForm() {
    return array();
  }

  /**
   * Nothing to submit.
   */
  public function settingsFormSubmit(&$form, &$form_state) {}

  /**
   * {@inheritdoc}
   */
  public function generateImage($object, $field, $instance, $bundle) {
    if ($fid = variable_get('did4dg_img_fid')) {
      return array('fid' => $fid, 'alt' => '', 'title' => '', 'width' => 0, 'height' => 0);
    }
  }
}
