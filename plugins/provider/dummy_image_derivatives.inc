<?php

/**
 * @file
 * CTools plugin of owner 'devel_image_provider' and type 'provider'.
 */

/**
 * Plugin info.
 *
 * @var array
 */
$plugin = array(
  'title' => t('Dummy image derivatives'),
  'class' => 'DummyImageDerivatives',
);
